var local = {
    'env': {
        name: 'development',
        serviceUrl: 'http://192.168.0.78:3003/',
        adminUrl: 'http://127.0.0.1:3004/',
        server: {
            host: '127.0.0.1',
            port: '3004'
        },
        database: {
            name: 'friendspire'
        }
    },
    'smtp': {
        host: 'smtp.gmail.com', //'smtp.yahoo.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'friendspire.debut@gmail.com',
            pass: 'security@01'
        }
    },
    'deeplink': {
        API_Key: "AIzaSyBjddy74wNnmpJjY8RCgtuVUCEbkm_hz1E",
        dynamicLinkDomain: "af2fx.app.goo.gl",
        androidPackageName: "com.android.bottledriver",
        androidFallbackLink: "https://play.google.com/store/apps/details?id=com.android.bottledriver",
        iosBundleId: ""
    },
    'push_notification': {
        server_key: 'AIzaSyC8ydaFMF_wq2YgCGtgoc-G5vV_a9k135I'
    },
    'timeformat': 'DD-MM-YYYY hh:mm A',
    'nearbyRaduis': 40000 / 6371000, // in meters max 100,000
    "post_expiry": 24,
    'from_email': "friendspire.debut@gmail.com"
};

var dev = {
    'env': {
        name: 'development',
        serviceUrl: 'http://js-server.debutinfotech.com:3003/',
        adminUrl: 'http://js-server.debutinfotech.com:3004/',
        server: {
            host: '127.0.0.1',
            port: '3004'
        },
        database: {
            name: 'friendspire'
        }
    },
    'smtp': {
        host: 'smtp.gmail.com', //'smtp.yahoo.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'friendspire.debut@gmail.com',
            pass: 'security@01'
        }
    },

    'push_notification': {
        server_key: 'AIzaSyC8ydaFMF_wq2YgCGtgoc-G5vV_a9k135I'
    },
    'timeformat': 'DD-MM-YYYY hh:mm A',
    'nearbyRaduis': 40000 / 6371000, // in meters max 100,000
    "post_expiry": 24,
    'from_email': "friendspire.debut@gmail.com"
};

var qa = {
    'env': {
        name: 'qa',
        serviceUrl: 'http://js-server.debutinfotech.com:3007/',
        adminUrl: 'http://js-server.debutinfotech.com:3006/',
        server: {
            host: '',
            port: '3007'
        },
        database: {
            name: 'friendspire_qa'
        }
    },
    'smtp': {
        host: 'smtp.gmail.com', //'smtp.yahoo.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'friendspire.debut@gmail.com',
            pass: 'security@01'
        }
    },
    'push_notification': {
        server_key: 'AIzaSyC8ydaFMF_wq2YgCGtgoc-G5vV_a9k135I'
    },
    'timeformat': 'DD-MM-YYYY hh:mm A',
    'nearbyRaduis': 40000 / 6371000, // in meters
    "post_expiry": 24, 
    'from_email': "friendspire.debut@gmail.com"

};

global.secret = "friendspire";
global.project_name = "Friendspire";
global.copyright_project_name = "Friendspire";
global.pagination_limit = 10;
global.app_info_email = "friendspire.debut@gmail.com";
global.fav_icon_path = "http://localhost:3004/images/icons/favicon.png";
global.logo_path = "http://localhost:3004/images/logo.png";
global.search_radius = 500;
global.admin = "5b84e07e1739cad237672997";
global.image = "/images/default.png"
let config = local;

module.exports = config;
/*var local = {
    'env': {
        name: 'development',
        serviceurl: 'http://127.0.0.1:3002/',
        adminurl: 'http://127.0.0.1:3001/',
        server: {
            host: '127.0.0.1',
            port: '3001'
        },
        database: {
            name: 'newAnother'
        }
    },
    'smtp': {
        host: 'smtp.gmail.com', //'smtp.yahoo.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'debut.developer@gmail.com',
            pass: 'debut@123'
        }
    },
    
    'push_notification': {
        server_key: 'AAAAA-Jhuvs:APA91bG5Q_qE_fz3i_QdFfKzs-6SeIZ6q_oRpW4RIXtK2C1_HxcuhXDIGbT5iLqX'
    },    
    'timeformat': 'DD-MM-YYYY hh:mm A',
    'from_email' : "nisheysaini9393@gmail.com"

};


var qa = {
    'env': {
        name: 'qa',
        serviceurl: '',
        adminurl: '',
        server: {
            host: '',
            port: ''
        },
        database: {
            name: 'demo'
        }
    },
    'smtp': {
        host: 'smtp.gmail.com', //'smtp.yahoo.com',
        port: 587,
        secure: false, // use SSL
        auth: {
            user: 'debut.developer@gmail.com',
            pass: 'debut@123'
        }
    },    
    'push_notification': {
        server_key: ''
    },    
    'timeformat': 'DD-MM-YYYY hh:mm A',
    'from_email' : "nisheysaini9393@gmail.com"
};

global.project_name = "Demo";
global.copyright_project_name = "demo";
global.app_info_email = "debut.developer@gmail.com";
global.fav_icon_path = "http://localhost:3001/images/icons/favicon.png";
global.logo_path = "http://localhost:3001/images/logo.png";

config = local;

if (process.env.NODE_ENV == 'qa') {
    config = qa;
}
module.exports = config;
*/