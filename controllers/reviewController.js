// const users = require('../models/user');
const reports = require('../models/report');
const recommendations = require('../models/recommendation');
var util = require('util');
var mongoose = require('mongoose');
var report = mongoose.model('report');
var moment = require('moment');

/**Get review list */
exports.reported_reviews_get = (req, res, next) => {
    new Promise((resolve, reject) => {

        console.log("req.query.....");
        console.log(req.query);
       
        var condition = {};
        var sortCondition = {};

        /** ***fetch conditions*****/
        if (req.query.search_by) {
            if (req.query.search_by.charAt(0) == '+') {
                req.query.search_by = req.query.search_by.slice(1);
            }
            req.query.search_by = req.query.search_by.trim();
            var search_by = new RegExp('.*' + req.query.search_by + '.*', 'i');
            condition.$or = [{ "user.firstName": search_by }, { "user.lastName": search_by }];
        }

        /** ***sorting conditions*****/
        if (req.query.sort_type) {
            if (req.query.sort_type == 1) { sortCondition.count = 1; }
            else if (req.query.sort_type == -1) { sortCondition.count = -1; }

        } else {
            sortCondition.created_at = -1;
        }

        console.log("condition");
        console.log(condition);
        console.log("sortCondition");
        console.log(sortCondition);
        /** ***pagination check*****/


        /** ***skip check*****/
        /*  var projection = {};
         var population = [{path: 'business_id', model: 'business_info'}]
  */

        let aggregation_condition = [
            {
                "$match": {
                    "$and": [
                        { "count": { $gt: 9 } },
                        { "is_deleted": { $ne: 1 } }

                    ]
                }
            },
            {
                "$lookup": {
                    "from": "recommendations",
                    "localField": "recommendation_id",
                    "foreignField": "_id",
                    "as": "recommend"
                }
            },
            {
                "$unwind": {
                    "path": "$recommend",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$match": {
                    "recommend.is_deleted": { $ne: 1 }

                }
            },
            {
                "$lookup": {
                    "from": "users",
                    "localField": "recommend.user_id",
                    "foreignField": "_id",
                    "as": "user"
                }
            },
            {
                "$unwind": {
                    "path": "$user",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$lookup": {
                    "from": "posts",
                    "localField": "recommend.reference_id",
                    "foreignField": "_id",
                    "as": "post"
                }
            },
            {
                "$unwind": {
                    "path": "$recommend",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$unwind": {
                    "path": "$post",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$project": {
                    "created_at": 1,
                    "updated_at": 1,
                    "recommend._id": 1,
                    "user.firstName": 1,
                    "user.lastName": 1,
                    "recommend.type": 1,
                    "post.title": 1,
                    "count": 1

                }
            },
            {
                $match: condition
            },

            {
                "$sort": sortCondition
            }
            //     ,
            //     { 
            //       "allowDiskUse" : false
            //  }


        ];

        var options_agg = { page: 1, limit: 10 };

        if (req.query.page) {
            options_agg.page = parseInt(req.query.page);
        }

        var aggregation_condition_data = report.aggregate(aggregation_condition);


        report.aggregatePaginate(aggregation_condition_data, options_agg)
            .then(function (results) {
                console.log("aggregation_condition_data")
                console.log(results)
                console.log(results, results.pageCount, results.totalCount);
                var pageCount = results.pageCount;
                var count = results.totalCount;
                // results = JSON.parse(JSON.stringify(results).replace(/null/ig, ""));
                var response = {};
                response.results = results;
                response.count = count;
                response.last = pageCount;
                // for search query
                if (req.query.page | req.query.search) {
                    response.current = parseInt(req.query.page || 1);

                    if (response.current > 1) {
                        response.prev = response.current - 1;
                    } else {
                        response.prev = response.current;
                    }
                    var pages = [];
                    if (pageCount <= 3) {
                        for (let i = 1; i <= pageCount; i++) {
                            pages.push(i);
                        }
                    } else {
                        for (let i = response.prev; i <= response.next; i++) {
                            pages.push(i);
                        }
                    }
                    response.pages = pages;
                    if (response.current < pageCount) {
                        response.next = response.current + 1;
                    } else {
                        response.next = response.current;
                    }
                    res.render('reviews/search', { response: response });
                } else {
                    console.log('first time');
                    response.current = 1;
                    response.prev = 1;
                    var pages = [];

                    if (pageCount <= 3) {
                        for (var i = 1; i <= pageCount; i++) {
                            pages.push(i);
                        }
                    } else {
                        for (var i = response.prev; i <= response.next; i++) {
                            pages.push(i);
                        }
                    }
                    response.pages = pages;
                    if (response.current < pageCount) {
                        response.next = response.current + 1;
                    } else {
                        response.next = response.current;
                    }
                    console.log((util.inspect(response, { depth: null })));
                    res.render('reviews/reviewList', {
                        response: response,
                        message: req.flash(),
                        title: 'Reviews',
                        delta: 4,
                        active: 'review_page'
                    });
                }
            })
            .catch(function (err) {
                console.log(err);
                if (req.query.search) {
                    return res.render('reviews/search', { response: [] });
                } else {
                    return res.render('reviews/search', {
                        response: [],
                        message: req.flash(),
                        title: 'Reviews',
                        active: 'review_page'
                    });
                }
            })
    }).catch((err) => {
        res.render("error", { error: err });
    })

}

//get review details
exports.get_review_detail = (req, res, next) => {
    new Promise((resolve, reject) => {
        console.log(req.params);
        // mongoose.Types.ObjectId(el)
        let id = mongoose.Types.ObjectId(req.params.id.toString());

        // let id = parseInt(req.params.id);
        console.log(id);
        let aggregation_condition = [
            {
                "$match": {
                    '_id': id
                }
            },
            {
                "$lookup": {
                    "from": "recommendations",
                    "localField": "recommendation_id",
                    "foreignField": "_id",
                    "as": "recommend"
                }
            },
            {
                "$unwind": {
                    "path": "$recommend",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$match": {
                    "recommend.is_deleted": { $ne: 1 }

                }
            },
            {
                "$lookup": {
                    "from": "users",

                    "localField": "recommend.user_id",
                    "foreignField": "_id",
                    "as": "user"
                }
            },
            {
                "$unwind": {
                    "path": "$user",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$lookup": {
                    "from": "posts",
                    "localField": "recommend.reference_id",
                    "foreignField": "_id",
                    "as": "post"
                }
            },
            {
                "$unwind": {
                    "path": "$post",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$project": {
                    "created_at": 1,
                    "updated_at": 1,
                    "user.firstName": 1,
                    "user.lastName": 1,
                    "user.email": 1,
                    "recommend._id": 1,
                    "recommend.type": 1,
                    "recommend.review": 1,
                    "post.title": 1,
                    "count": 1

                }
            }

        ];

        reports.aggregate(aggregation_condition).then((review) => {
            if (review.status === 1) {
                console.log("reviews");
                console.log(review);
                let i = 0;
                review.data[i].updated_at = moment(review.data[i].updated_at).utcOffset(req.cookies.time_zone_offset).format('DD-MM-YYYY hh:mm A');

                console.log("review..........");
                console.log((util.inspect(review.data[0], { depth: null })));
                res.render('reviews/reviewDetail', { status: 1, message: req.flash(), response: review.data[0], title: 'Review Detail', active: 'review_page' });
            }
            else {
                res.redirect('/reviews');
            }
        }).catch((err) => {
            res.render("error", { error: err });
        })


    }).catch(err => {
        console.log(err);
        res.render('error', { error: err });
    });
};


//delete review from recommend
exports.delete_review = (req, res, next) => {
    new Promise((resolve, reject) => {

        console.log(req.params.id);
        let id = mongoose.Types.ObjectId(req.params.id.toString());

        let condition = {
            _id: id
        }
        let update = {
            is_deleted: 1
        }
        recommendations.update(condition, update).then((recommend) => {
            console.log(recommend);
            if (recommend.status === 1) {
                req.flash('success', 'recommendation has been deleted successfully');
                res.redirect('/reviews');
            }
            else {

                req.flash('error', recommend.message);
                res.redirect('/reviews');
            }

        }).catch((err) => {
            reject(err);
        })


    }).catch((err) => {
        res.render("error", { error: err });
    })
}

//mark appropriate by setting is_delted in report tables
exports.mark_appropriate = (req, res, next) => {
    new Promise((resolve, reject) => {
        console.log("check..req.params.id");
        console.log(req.params.id);
        let id = mongoose.Types.ObjectId(req.params.id.toString());
        console.log(id)
        let condition = {
            _id: id
        }
        let update = {
            is_deleted: 1
        }
        reports.update(condition, update).then((report) => {
            console.log(report);
            if (report.status === 1) {
                req.flash('success', 'report has been marked as appropriated');
                res.redirect('/reviews');
            }
            else {

                req.flash('error', report.message);
                res.redirect('/reviews');
            }
        }).catch((error) => {
            reject(error);
        });
    }).catch((error) => {
        req.flash('error', error);
        res.redirect('/reviews');
    });
}