"use strict";
const Promise = require('bluebird');


const users = require('../models/user.js');
const moment = require('moment');
const mails = require('../helper/send_mail.js');
const email = require('../email_template_cms_pages');
const email_templates = require('../models/email_template.js');


/** get user listing */
exports.get_users = (req, res, next) => {

    console.log("queryyyyyyyyyyyy");
    console.log(req.query);
    new Promise((resolve, reject) => {
        // make global variable options for paginate method parameter
        let options = {
            perPage: 10,
            delta: 4,
            page: 1
        };
        /** ***condition for not deleted users*****/
        let condition = {
            type: { $ne: 1 },
            status: { $ne: 2 },
            is_deleted: 0
        };
        let sortCondition = {};
        /** ***fetch conditions*****/
        var searchby = req.query.search_by;
        if (searchby) {
            if (searchby.charAt(0) === '+') {
                searchby = searchby.slice(1);
            }
            searchby = searchby.trim();
            let search_array = searchby.split(' ');
            if (search_array.length === 1) {
                 var search_by = new RegExp('.*' + searchby + '.*', 'i');
                condition.$or = [{ firstName: search_by }, { lastName: search_by }, { email: search_by }];
            } else {
                let firstName = new RegExp(search_array[0], 'i');
                let lastName = new RegExp('.*' + search_array[1] + '.*', 'i');
                condition.$and = [{ firstName: firstName }, { lastName: lastName }, { email: search_by }];
            }
        }

        /** add conditons if gender and status is added */

        if (req.query.status && req.query.gender) {
            if (req.query.status != "all") {
                condition.status = req.query.status;
            }
            if (req.query.gender != "all") {
                condition.gender = req.query.gender;
            }
        }
        else if (req.query.status) {
            if (req.query.status != "all") {
                condition.status = req.query.status;
            }
        }
        else if (req.query.gender) {
            if (req.query.gender != "all") {
                condition.gender = req.query.gender;
            }
        }



        /** ***pagination check*****/
        if (req.query.page) {
            options.page = req.query.page;
        }
        /** ***skip check*****/
        let skipRecords = options.page - 1;
        let projection = {
            firstName: 1,
            lastName: 1,
            gender: 1,
            email: 1,
            type: 1,
            status: 1,
            created_at: 1,
            is_deleted: 1
        };
        let population = [];

        users.find_with_pagination_projection(
            condition,
            projection,
            population,
            sortCondition,
            options.perPage * skipRecords,
            options.perPage,
            options).then((user_data) => {

                if (user_data.status === 1) {
                    let userData = user_data.data;
                    console.log(userData);
                    // for search query
                    if (req.query.page || req.query.search) {
                        res.render('users/search', { response: userData });
                    } else // for simple query
                    {
                        res.render('users/usersList', {
                            response: userData,
                            message: req.flash(),
                            title: 'Users',
                            delta: 4,
                            active: 'user_page'
                        });
                    }
                } else {
                    res.redirect('/users');
                }
            });
    }).catch(err => {
        res.redirect('/users');
    });
};

/**get user details */
exports.get_user_detail = (req, res, next) => {
    new Promise((resolve, reject) => {
        let id = req.params.id;
        users.findOne({ _id: id, is_deleted: 0 }).then((user_data) => {
            if (user_data.status === 1) {
                let userDetailData = JSON.parse(JSON.stringify(user_data.data));
                console.log("userDetailData...............");
                console.log(userDetailData);
                userDetailData.created_at = moment(userDetailData.created_at).utcOffset(parseInt(req.cookies.time_zone_offset)).format(global.timeformat);
                // res.render('users/userDetail', {message: req.flash(), back_url: req.query.refer, userDetailData: userDetailData, title: 'User Detail', active: 'user_page'});
                res.send({ status: 1, message: req.flash(), back_url: req.query.refer, userDetailData: userDetailData, title: 'User Detail', active: 'user_page' });
            } else {
                res.send({ status: 0 });
            }
        });
    }).catch(err => {
        console.log(err);
        res.render('error', { error: err });
    });
};

/**enable/disable user */
exports.post_disable_user = (req, res, next) => {
    new Promise((resolve, reject) => {
        let id = req.body.userId;
        var status = req.body.status;
        console.log(status);
        let remarks = req.body.disable_reason;
        users.findOne({ _id: id, is_deleted: 0 }).then(client_data => {
            if (client_data.status === 1) {
                var user_status;
                if (status == 1) {
                    user_status = 'unblocked';
                }
                
                if (status == 0) {
                    user_status = 'blocked';
                }


                let temp_id = email.enable_disable_user;
                email_templates.findOne({ _id: temp_id }).then(template => {
                    if (template.status === 1) // && client_data.data.is_email_verified === 1
                    {
                        var content = template.data.content;
                        content = content.replace('@name@', client_data.data.firstName);
                        content = content.replace('@status@', user_status);
                        content = content.replace('@remarks@', remarks);
                        let subject = template.data.subject;
                        subject = subject.replace('@status@', user_status);
                        mails.send(client_data.data.email, subject, content);
                    }
                    users.update({ _id: id }, { $set: { status: req.body.status } }).then(() => {
                        req.flash('success', 'User has been ' + user_status + ' successfully');
                        res.send({ 'status': 1 });
                    });
                });
            } else {
                res.send({ 'status': 0, message: client_data.message });
            }
        });
    }).catch(err => {
        console.log(err);
        res.send({ 'status': 0, message: err.message });
    });
};

