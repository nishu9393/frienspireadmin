module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": ["eslint:recommended", "standard"],
    "rules": {
        "indent": [
            "error",
            4,
            {
                "SwitchCase": 1
            }
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "camelcase": "off",
        "no-console": "off",
        "space-before-function-paren": "off",
        "indent": "off",
        "no-multiple-empty-lines": "off",
        "quotes": "off",
        "semi": "off",
        "eol-last": "off",
        "no-trailing-spaces": "off",
        "padded-blocks": "off",
        "eqeqeq": "off",
        "keyword-spacing": "off",
        "one-var": "off",
        "brace-style": "off",
        "spaced-comment": "off",
        "object-curly-spacing": "off",
        "arrow-spacing": "off",
        "space-in-parens": "off",
        "comma-spacing": "off",
        "prefer-promise-reject-errors": "off"
    }
};
