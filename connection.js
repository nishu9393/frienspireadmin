var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var config = require('./config');
var db_name = config.env.database.name;
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

var cms_page = new Schema({
    name: {type: String},
    heading: {type: String, required: [true, 'Please enter Question']},
    description: {type: String},
    type: {type: Number, default: 0}, // 1- pages, 2-faqs
    status: {type: Number, default: 1}
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

// defines email_template schemas
var email_template = new Schema({
    name: {type: String, required: [true, "name is required"]},
    subject: {type: String, required: [true, "subject is required"]},
    content: {type: String, required: [true, "content is required"]},
    status: {type: Number, enum: [0, 1], default: 1}
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var password_reset = new Schema({
    email: String,
    reset_token: String,
    expiry: Date // expiry is 30 min
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var notification = new Schema({
    reference_id: {type: String,default: ""},
    from: {type: Schema.Types.ObjectId, ref: 'user'},
    to: {type: Schema.Types.ObjectId, ref: 'user'},
    type: {type: Number, default: 0}, // 1-accept, 2-reject, 3-report, 4-follow request, 5-contact_us
    message: {type: String},
    is_deleted: {type: Number, default: 0}, //1-deleted
    is_read: {type: Number, default: 0}
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});


// defines user schemas
var user = new Schema({
    firstName: {type: String, default: ""},
    lastName: {type: String, default: ""},
    sns_type: {type: Number, default: 1}, // 1-email,2-facebook
    sns_id: {type: String, default: ""},
    fb_connected: {type: Number, default: 0},
    email: {type: String, match: /^([^@]+?)@(([a-z0-9]-*)*[a-z0-9]+\.)+([a-z0-9]+)$/i},
    temp_email: {type: String, default: ""},
    status: {type: Number, default: 0}, // 0-inactive,1-active,2-otp_verification_pending
    password: {type: String},
    phone: {type: String},
    country_code: {type: String},
    gender: {type: Number}, //1-male,2-female,3-others
    dob: {type: Date},
    otp_code: {type: String, default: ""},
    otp_expiry: Date, // otp expiry is 30 min
    forgot_otp_code: {type: String, default: ""},
    forgot_otp_expiry: Date, // otp expiry is 30 min
    email_otp_code: {type: String, default: ""},
    email_otp_expiry: Date, // otp expiry is 30 min
    type: {type: Number, default: 2, enum: [1, 2]}, // 1-admin,2-appuser
    profile_pic: {type: String, default: ""},
    push_notification: {type: Number, default: 1},
    device_token: {type: String, default: ""},
    is_email_verified: {type: Number, default: 0},
    is_email_changed: {type: Number, default: 0},
    is_deleted: {type: Number, default: 0}, //1-deleted by admin
    is_address_added: {type: Number, default: 0},
    address: {type: String, default: ""},
    longlat: [{type: Number}],
    friends: [{user_id: {type: Schema.Types.ObjectId, ref: 'user'},
            status: {type: Number, default: 0}, // 0 - default , 1- follow, 2- request sent
            type: {type: Number, default: 2} // 1-facebook,2-by search
        }],
    distance_unit: {type: Number, default: 1}, //   1- miles 2-kilometer
    last_login: {type: Number},
    is_private: {type: Number, default: 1},   
    latest_token: {type: String}
   
});

var post = new Schema({
    type: {type: Number, default: 0}, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    reference_id: {type: String},
    title: {type: String, default: ""},
    start_year: {type: Number},
    end_year: {type: Number},
    year: {type: Number}, //
    genre: {type: [String]},
    rated: {type: String},
    author: {type: String}, //author -> book,writer -> movie
    director: {type: String}, //author -> book,writer -> movie
    actors: {type: String},
    friendspire_rating: {type: Number, default: 0},
    rating: {type: Number, default: 0},
    image: [{type: String, default: ""}],
    language: {type: [String]},
    publisher: {type: String},
    description: {type: String, default: ""}, //plot in case of book
    isbn: {type: String},
    awards: {type: String},
    pages_duration: {type: Number}, //page -> book, duration -> movies and series(minutes)
    country: {type: [String]},
    city: {type: String},
    address: {type: [String]},
    release_date: {type: Date},
    rated_at: {type: Date},
    location: [{coordinates: {type: [Number], index: '2dsphere'}}], //store in the form of long lat
    price_tier: {type: Number},
    phone: {type: String, default: ""},
    url: {type: String, default: ""},
    cuisine: [{type: String}],
    timings: [{type: mongoose.Schema.Types.Mixed}],
    timezone: {type: String, default: ""},
//    bookmark: [{type: Schema.Types.ObjectId, ref: 'user'}],
    expiry_time: Number // exiration time for the recordes 
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var recommendation = new Schema({
    type: {type: Number}, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    rating: {type: Number, default: 0},
    review: {type: String, default: ""},
    user_id: {type: Schema.Types.ObjectId, ref: 'user'},
    reference_id: {type: Schema.Types.ObjectId, ref: 'post'},
    is_private: {type: Number, default: 0}, //1-private
    is_deleted: {type: Number, default: 0} //1-deleted
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var bookmark = new Schema({
    type: {type: Number}, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
    status: {type: Number, default: 0}, //1-bookmark added, 0-bookmark remove
    user_id: {type: Schema.Types.ObjectId, ref: 'user'},
    reference_id: {type: Schema.Types.ObjectId, ref: 'post'}
    //is_removed: {type: Number, default: 0}  //1-deleted
    //count: { type: Number, default: 0}
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var report = new Schema({
    recommendation_id: {type: Schema.Types.ObjectId, ref: 'recommendation'},
    count: {type: Number},
    users: [{type: Schema.Types.ObjectId, ref: 'user'}],
    is_deleted: {type: Number, default: 0} //1-deleted
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
report.plugin(mongooseAggregatePaginate);

// var notification = new Schema({ 
//     from: {type: Schema.Types.ObjectId, ref: 'user'},
//     to: {type: Schema.Types.ObjectId, ref: 'user'},
//     is_deleted: {type: Number, default: 0},  //1-deleted
//     is_read: {type: Number, default: 0},  //1-read
//     type: {type: Number}, //1-restaurant,2-bar,3-Movie,4-TV,5-Book
//     report_id: {type: Schema.Types.ObjectId, ref: 'report'}
// }, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

var contact_category = new Schema({
    name: {type: String, required: [true, 'Please enter name']},
    status: {type: Number, default: 1},
    type: {type: Number},
    is_deleted: {type: Number, default: 0}
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});

// var contact_us = new Schema({
//     name: {type: String},
//     email: {type: String},
//     user_id: {type: Schema.Types.ObjectId, ref: 'user'},
//     subject: {type: Schema.Types.ObjectId, ref: 'contact_category'},
//     message: {type: String},
//     is_read: {type: Number, default: 0},
//     status:{type: Number, default: 0}, //0 -unresolved , 1- resolved
// }, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
var contact_us = new Schema({
    user_id: {type: Schema.Types.ObjectId, ref: 'user'},
    subject: {type: Schema.Types.ObjectId, ref: 'contact_category'},
    message: {type: String,default: ""},
    is_deleted: {type: Number, default: 0},
    status: {type: Number, default: 0} //0 -unresolved , 1- resolved
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
contact_us.plugin(mongooseAggregatePaginate);

var setting = new Schema({
    ios_version: { type: Number },
    android_version: { type: Number }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

var email_template_attributes = new Schema({
    email_template_id: String,
    variable: { type: String, required: [true, 'Variable is required'] },
    deleted_at: String
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

mongoose.model('user', user);
mongoose.model('email_template', email_template);
mongoose.model('password_reset', password_reset);
mongoose.model('cms_page', cms_page);
mongoose.model('post', post);
mongoose.model('recommendation', recommendation);
mongoose.model('bookmark', bookmark);
mongoose.model('report', report);
mongoose.model('notification', notification);
mongoose.model('contact_category', contact_category);
mongoose.model('contact_us', contact_us);
mongoose.model('setting', setting);
mongoose.model('email_template_attributes', email_template_attributes);

var mongoUrl = 'mongodb://127.0.0.1:27017/' + db_name;
// mongoose runs only on 27017 port

var options = {
    useMongoClient: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 5000 // Reconnect every 500ms
    // poolSize: 10, // Maintain up to 10 socket connections
    // // If not connected, return errors immediately rather than waiting for reconnect
    // bufferMaxEntries: 0
};

var connectWithRetry = function () {
    return mongoose.connect(mongoUrl, options, function (err) {
        if (err) {
            console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
            setTimeout(connectWithRetry, 5000);
        } else {
            console.log('Connected to ' + db_name + ' database.');
        }
    });
};

connectWithRetry();

