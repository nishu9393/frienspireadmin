var mongoose = require('mongoose');
var report = mongoose.model('report');

exports.findOne = (query) => {
    return new Promise((resolve,reject) => {
        report.findOne(query , (error, data) => {
            if (!error) {
                if (data) {
                    resolve({ status: 1, message: "user already reported"});
                } else {
                    resolve({ status: 0, message: "user can report"});
                }
            } else {
               resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.aggregate = function (aggregate_query) {
    return new Promise((resolve,reject) => {
        report.aggregate(aggregate_query, (error, data) => {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                resolve({status: 1, message: "success", data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};
exports.update = (query, update_data) => {
    return new Promise((resolve,reject) => {
        report.update(query, {$set: update_data} , function (error, data) {

            if (!error) {
                if (data.nModified > 0) {
                    resolve( { status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "report data could not update because report already deleted" });
                }
            } else {
                resolve( { status: 0, message: error.message });
            }
        });
    });
};
exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        // query.is_deleted = 0;
        report.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        }).catch(err => reject(err));
    });
};

// exports.findOneAndUpdate = function (query, update_data, callback) {
//     process.nextTick(function () {
//         report.findOneAndUpdate(query, update_data, { upsert: true, new: true }, function (error, data) {

//             if (!error) {
//                 if (data !== null) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Review is not reported" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };