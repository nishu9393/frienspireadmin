var mongoose = require('mongoose');
var bookmark = mongoose.model('bookmark');


exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        // query.is_deleted = 0;
        bookmark.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        }).catch(err => reject(err));
    })
};

// exports.UpdateAndSave = function (query, update_data, callback) {
//     process.nextTick(function () {
//         bookmark.findOneAndUpdate(query, update_data, {upsert: true, new : true}, function (error, data) {
//             if (!error) {
//                 if (data !== null) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, {status: 0, message: "Users data could not update"});
//                 }
//             } else {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };

// exports.findWithPagination = function (query, projection, sort_condition, skip, limit, callback) {
//     process.nextTick(function () {
//         bookmark.find(query, projection).populate('reference_id').sort(sort_condition).skip(skip).limit(limit).exec(function (error, data) {
//             if (!error) {
//                 if (data) {
//                     data = JSON.parse(JSON.stringify(data));
//                     callback(null, {status: 1, message: "success", data: data});
//                 } else {
//                     callback(null, {status: 0, message: "Users not found"});
//                 }
//             } else {
//                 callback(null, {status: 0, message: error.message});
//             }
//         });
//     });
// };


// exports.count = function (query) {
//     return new Promise((resolve, reject) => {
//         bookmark.count(query, (error, result) => {
//             if (error) {
//                 reject(error);
//             } else {
//                 resolve(result);
//             }
//         });
//     });
// };
// exports.findOneAndUpdate = function (query, update_data, callback) {
//     process.nextTick(function () {
//         bookmark.findOneAndUpdate(query, update_data, { new: true }, function (error, data) {

//             if (!error) {
//                 if (data !== null) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Users data could not update" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };

// exports.findOne = function (query, callback) {
//     process.nextTick(function () {
//         bookmark.findOne(query, function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Data not found", data: {} });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };


// exports.update = function (query, update_data, callback) {
//     process.nextTick(function () {
//         bookmark.update(query, update_data, function (error, data) {

//             if (!error) {
//                 if (data.nModified > 0) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Bookmark can't be updated" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };



// exports.aggregate = function (aggregate_query, callback) {
//     process.nextTick(function () {
//         bookmark.aggregate(aggregate_query, function (error, data) {
//             if (!error) {
//                 data = JSON.parse(JSON.stringify(data));
//                 console.log("data bookmark");
//                 console.log(data);
//                 callback(null, { status: 1, message: "success", data: data });
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };
// exports.find = function (query, projection, callback) {
//     process.nextTick(function () {
//         bookmark.find(query, projection, function (error, data) {
//             if (!error) {
//                 if (data) {
//                     callback(null, { status: 1, message: "success", data: data });
//                 } else {
//                     callback(null, { status: 0, message: "Data not found" });
//                 }
//             } else {
//                 callback(null, { status: 0, message: error.message });
//             }
//         });
//     });
// };
