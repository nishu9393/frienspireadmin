
"use strict";
const mongoose = require("mongoose");
const user = mongoose.model("user");


exports.findOne = (query, projection) => {
   return new Promise((resolve,reject) =>{
        user.findOne(query, projection, (error, data) => {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve( { status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "User not found" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    })
          
};
exports.findOne_population = (query, populate_data) => {
    return new Promise((resolve,reject)=> {
    user.findOne(query).populate(populate_data).exec((error, data) => {
        if (!error) {
            if (data.length !== 0) {
                data = JSON.parse(JSON.stringify(data));
                resolve( { status: 1, message: "success", data: data });
            } else {
                resolve({ status: 2, message: "Users not found" });
            }
        } else {
            resolve({ status: 0, message: error.message });
        }
    });

  })
          
};

exports.aggregate = (aggregate_query) => {
    return new Promise((resolve,reject)=> {
        user.aggregate(aggregate_query, (error, data) => {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
               resolve( { status: 1, message: "success", data: data });

            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.find = (query, projection) => {
    return new Promise((resolve,reject)=> {
        user.find(query, projection, (error, data) => {
            if (!error) {
                if (data) {
                    resolve( { status: 1, message: "success", data: data });
                } else {
                    resolve( { status: 0, message: "Users not found" });
                }
            } else {
                resolve( { status: 0, message: error.message });
            }
        });
    });
};


exports.find_pagination = (query, projection, skip, limit) => {
    return new Promise((resolve,reject)=> {
        user.find(query, projection).skip(skip).limit(limit).exec( (error, data) => {
            if (!error) {
                if (data) {
                    resolve( { status: 1, message: "success", data: data });
                } else {
                    resolve( { status: 0, message: "Users not found" });
                }
            } else {
                resolve( { status: 0, message: error.message });
            }
        });
    });
};

exports.find_pagination_sort = (query, projection, skip, limit,sort) => {
    return new Promise((resolve,reject)=> {
        user.find(query, projection).skip(skip).limit(limit).sort(sort).exec( (error, data) => {
            if (!error) {
                if (data) {
                    resolve({ status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "Users not found" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};
exports.save = (user_data) => {
    return new Promise((resolve,reject)=> {
        var new_user = new user(user_data);
        new_user.save((error, data) => {
            if (!error) {
                data = JSON.parse(JSON.stringify(data));
                resolve( { status: 1, message: "success", data: data });
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.update = (query, update_data) =>{
    return new Promise((resolve,reject)=> {
        user.update(query, update_data, { multi: true }, (error, data) => {
            if (!error) {
                if (data.nModified > 0) {
                    resolve({ status: 1, message: "success" });
                } else {
                    resolve({ status: 0, message: "Users data could not update" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.findOneAndUpdate = (query, update_data) => {
    return new Promise((resolve,reject)=> {
        user.findOneAndUpdate(query, update_data, { new: true }, (error, data) => {
            if (!error) {
                if (data !== null) {
                    resolve({ status: 1, message: "success", data: data });
                } else {
                    resolve({ status: 0, message: "Users data could not update" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

//remove record
exports.remove = (query) => {
    return new Promise((resolve,reject)=> {
        user.remove(query, (error, data) => {
            if (!error) {
                resolve({ status: 1, message: "success" });
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.count = (query) => {
    return new Promise((resolve,reject)=> {
        user.count(query, (error, data) => {
            if (!error) {
                if (data[0] !== null) {
                    resolve({ status: 1, message: "success", data: data });
                } else {
                    resolve( { status: 0, message: "Match not found" });
                }
            } else {
                resolve({ status: 0, message: error.message });
            }
        });
    });
};

exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        // query.is_deleted = 0;
        user.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        }).catch(err => reject(err));
    });
};

exports.find_with_pagination_projection = (query, projection, population, sort_condition, skip, limit, options) => {
    return new Promise((resolve,reject) => {
        user.find(query, projection).sort(sort_condition)
                .populate(population)
                .skip(skip).limit(limit).paginate(options,(error, data) => {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'Users not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.find_with_projection = (query, projection) => {
    return new Promise((resolve,reject) => {
            query.is_deleted = 0;
            query.type = 2;
            user.find(query, projection,(error, data) => {
                if (!error) {
                    if (data.length !== 0) {
                        data = JSON.parse(JSON.stringify(data));
                        resolve({status: 1, message: 'success', data: data});
                    } else {
                        resolve({status: 2, message: 'Users not found'});
                    }
                } else {
                    resolve({status: 0, message: error.message});
                }
            });
        }).catch(err => { // catch errors
            return Promise.resolve({status: 0, message: err.message});
        });
};
/*
exports.findOne = (query) => {
    return new Promise((resolve,reject) => {
        user.findOne(query, {__v: 0, updated_at: 0},(error, data) => {
            if (!error) {
                if (data !== null) {
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'User not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};


exports.findOne_projection_population = function (query, projection, populate_query, callback) {
    process.nextTick(function () {
        query.is_deleted = 0;
        user.findOne(query, projection).populate(populate_query).exec(function (error, data) {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: 'success', data: data});
                } else {
                    callback(null, {status: 2, message: 'User not found'});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.find_projection_population = (query, projection, populate_query, sort_condition) => {
    return new Promise((resolve, reject) => {
        query.is_deleted = 0;
        user.find(query, projection).populate(populate_query).sort(sort_condition).exec((error, data) => {
            if (!error) {
                if (data !== null) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'User not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.find = function (query, sort_condition, callback) {
    process.nextTick(function () {
        query.is_deleted = 0;
        user.find(query).sort(sort_condition).exec(function (error, data) {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: 'success', data: data});
                } else {
                    callback(null, {status: 2, message: 'Users not found'});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.find_count = (query) => {
    return new Promise((resolve,reject) => { 
        query.is_deleted = 0;
        user.find(query).count().exec((error, data) => {
            if (!error) {
                resolve({status: 1, message: 'success', data: data});
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    }).catch(err => console.log(err));
};
exports.find_with_pagination = function (query, sort_condition, skip, limit, options, callback) {
    process.nextTick(function () {
        user.find(query).sort(sort_condition).skip(skip).limit(limit).paginate(options, function (error, data) {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: 'success', data: data});
                } else {
                    callback(null, {status: 2, message: 'Users not found'});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};
exports.find_with_pagination_projection = (query, projection, population, sort_condition, skip, limit, options) => {
    return new Promise((resolve,reject) => {
        user.find(query, projection).sort(sort_condition)
                .populate(population)
                .skip(skip).limit(limit).paginate(options,(error, data) => {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    resolve({status: 1, message: 'success', data: data});
                } else {
                    resolve({status: 2, message: 'Users not found'});
                }
            } else {
                resolve({status: 0, message: error.message});
            }
        });
    });
};

exports.findOne_with_projection = function (query, projection, callback) {
    process.nextTick(function () {
        query.is_deleted = 0;
        user.findOne(query, projection).exec(function (error, data) {
            if (!error) {
                if (data.length !== 0) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: 'success', data: data});
                } else {
                    callback(null, {status: 2, message: 'User not found'});
                }
            } else {
                callback(null, {status: 0, message: error.message});
            }
        });
    });
};

exports.find_with_population = function (query, populate_data, sort_condition, callback) {
    process.nextTick(function () {
        try {
            query.is_deleted = 0;
            user.find(query).populate(populate_data).sort(sort_condition).exec(function (error, data) {
                if (!error) {
                    if (data.length !== 0) {
                        data = JSON.parse(JSON.stringify(data));
                        callback(null, {status: 1, message: 'success', data: data});
                    } else {
                        callback(null, {status: 2, message: 'Users not found'});
                    }
                } else {
                    callback(null, {status: 0, message: error.message});
                }
            });
        } catch (err) // catch errors
        {
            callback(null, {status: 0, message: err.message});
        }
    });
};

exports.find_with_projection = (query, projection) => {
    return new Promise((resolve,reject) => {
            query.is_deleted = 0;
            query.type = 2;
            user.find(query, projection,(error, data) => {
                if (!error) {
                    if (data.length !== 0) {
                        data = JSON.parse(JSON.stringify(data));
                        resolve({status: 1, message: 'success', data: data});
                    } else {
                        resolve({status: 2, message: 'Users not found'});
                    }
                } else {
                    resolve({status: 0, message: error.message});
                }
            });
        }).catch(err => {// catch errors
            return Promise.resolve({status: 0, message: err.message});
        });
};

exports.find_with_near_geometry = function (coordinates, distance, condition, projection, skip_count, limit_stylist, callback) {
    process.nextTick(function () {
        try {
            user.find({$and: [{business_address_coordinates: {$near: {$geometry: {type: 'Point', coordinates: coordinates}, $maxDistance: distance}}, is_deleted: 0, status: 1}, condition]}, projection).skip(skip_count).limit(limit_stylist).exec(function (error, data) {
                if (!error) {
                    data = JSON.parse(JSON.stringify(data));
                    callback(null, {status: 1, message: 'success', data: data});
                } else {
                    console.log(error);
                    callback(null, {status: 0, message: error.message});
                }
            });
        } catch (err) // catch errors
        {
            callback(null, {status: 0, message: err.message});
        }
    });
};

exports.update = (query, update_data) => {
    return new Promise((resolve,reject) => {
            query.is_deleted = 0;
            user.update(query, update_data, {multi: true},(error, data) => {
                if (!error) {
                    if (data.nModified > 0) {
                        resolve({status: 1, message: 'success'});
                    } else {
                        resolve({status: 2, message: 'Users data could not update'});
                    }
                } else {
                    resolve({status: 0, message: error.message});
                }
            });
        }).catch (err => {// catch errors
            resolve({status: 0, message: err.message});
        });
};

exports.remove = function (query, callback) {
    process.nextTick(function () {
        try {
            user.remove(query, function (error, data) {
                if (!error) {
                    console.log('user removed');
                    console.log(data.result);
                    callback(null, {status: 1, message: 'success'});
                } else {
                    callback(null, {status: 0, message: error.message});
                }
            });
        } catch (err) // catch errors
        {
            callback(null, {status: 0, message: err.message});
        }
    });
};

*/











