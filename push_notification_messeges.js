// all push notification messages
module.exports = {
    user_module: {       
        'disable_by_admin': 'Your account has been blocked by admin. Please contact us for more information.',
        'deleted_by_admin': 'Your account has been deleted by admin.'       
    },
    admin_module: {        
        'contact_us_query': 'You have recieved a new query from @name@.'
    }
};
