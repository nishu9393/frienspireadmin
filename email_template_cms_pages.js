// all email template ids
module.exports = {
    forgot_password: '57877a5696583ebc4f0dc41d',
    forgot_password_admin: '5b8507da1739cad237673e8b',
    password_changed: '5b86179ff94993f3ac4ce50b',
    email_confirmation: "5b8e13bcae546b45851945d2",
    otp_verification: '5b8e1503ae546b45851946aa',
    forgot_password_app_user: '59c25013adc0a46b01c4ae38',
    enable_disable_user: '5b862554f94993f3ac4ceda3',
    policy: "57d69e92897a866611292130",
    condition: "57cfb659f5f6a7b55b958e78",
    invite_user_email: "582599f8c176395c9b2e7921",
    thank_you_mail_contact: "59c8a09f80c4441765bc6e3e",
    contact_us_admin: "59c8a04180c4441765bc6e3a",
    //  otp_verification: "59c24e6fadc0a46b01c4ae35",
    term_condition: "591431f76cd6f8791596e72b",
    privacy_policy: "591432c1d4968e7bda6c78c5",
    sign_otp_email: "59aa54900af81178d6e0b581",
    email_verification: "5af14a6073e2c323f1a91fd1",
    cms_pages: {
        termsConditions: "59bfb3d3a5e7083b8669b9e5",
        privacy_policy: "5a1e5af64a2e827b73226e27",
        aboutUs: "5b4d8aab033ededce1571d3f"
    }
};
// module.exports = {
//     email_templates: {
//         welcome_mail: '5a27ca5b434b6570d3b17c9f',
//         password_changed: '5b7e39c6cf49a93cc8d7efa3',
//         forgot_password_admin: '5ae6aa9f23314d20a8787c23',
//         contact_us_mail_for_admin: '5a27cb31434b6570d3b17ca5',
//         Thanks_for_contacting_us: '5a27cb79434b6570d3b17ca9',
//         enable_disable_user: '5a27cbca434b6570d3b17cab',
//         registration_email_otp: '5a27d06649ccf279be3bc1b3',
//         account_restored: '5a27cbfa434b6570d3b17caf',
//         email_verification: '5b7fa398c4fbdd2db34d46ec',
//         email_confirmation: '5b7fb79deac7fe3bda777e81',
//         phone_changed: '5a27cd71434b6570d3b17cba',
//         email_changed: '5a27cda0434b6570d3b17cbc',
//         block_drive_by_admin: '5a4c9bc1b1fcf822a9aa349a',
//         delete_by_admin: '5a4c9a304125461912dd3435',
//         payment_recieved: '5a54442c047c4d0e8743c1c4'
//     },
//     cms_pages: {
//         term_policy: '5a27d243da7bb47a32377b94',
//         bottle_driver_type: '5a4c9cc799cf4125f5064d5e'
//     }
// };
