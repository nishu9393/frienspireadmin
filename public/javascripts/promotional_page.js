/*---------------------------------------------------------*/
/*  CONTACT FORM                                           */
/*---------------------------------------------------------*/

$("#contact_form_blog").submit(function (e) {
    e.preventDefault();
    var btn = $('#submit');
    btn.button('loading');

    btn.button('reset');

    var b = 'border-error';
    var ap = 'animated-error pulse';
    var bap = 'border-error animated-error pulse';

    //specifing language of the form..
    var language = $("#language").val();

    var n = '#name';
    var name = $("#name").val();

    var e = '#email';
    var email = $("#email").val();

    var m = '#message';
    var message = $("#message").val();

    var c = '#captcha';
    var captcha = $("#captcha").val();

    var data_to_send = {name: name, email: email, message: message, captcha: captcha, language: language};


    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }

    if (name.length <= 1) {
        $(n).addClass(bap);
        setTimeout(function () {
            $(n).removeClass(ap);
        }, 1000);
    } else {
        $(n).removeClass(b);
    }

    if (isValidEmail(email) === false) {
        $(e).addClass(bap);
        setTimeout(function () {
            $(e).removeClass(ap);
        }, 1000);
    } else {
        $(e).removeClass(b);
    }

    if (message.length <= 1) {
        $(m).addClass(bap);
        setTimeout(function () {
            $(m).removeClass(ap);
        }, 1000);
    } else {
        $(m).removeClass(b);
    }

    if (captcha.length <= 1) {
        $(c).addClass(bap);
        setTimeout(function () {
            $(c).removeClass(ap);
        }, 1000);
    } else {
        $(c).removeClass(b);
    }

    if (isValidEmail(email) && (message.length > 1) && (name.length > 1) && (captcha.length > 1)) {
   
        //$('#submit').attr('disabled','disabled');
        $.ajax({
            type: "POST",
            url: "/",
            data: data_to_send,
            success: function (response) {
                console.log(response);
                if (response.status === 1) {
                
                    $(btn).fadeOut(500);
                    $('.success').text(response.message);
                    $('.success').fadeIn(1000);
                    $(n, e, m).removeClass(b);
                } else {
                    /*replace the captcha image */
                    if (response.status === 0)
                        $("#image_captcha").attr('src', 'data:image/jpeg;base64,' + response.valicode);
                    //clear captcha value
                    $("#captcha").val("");
                 
                    $('.success').text(response.message);
                    $('.success').fadeIn(1000);
                    $(n, e, m).removeClass(b);
                }
            }
        });

    }
    return false;

});


$("#refresh").click(function () {

    $.ajax({
        url: "/recaptcha",
        type: "GET",
        success: function (result) {
            if (result.valicode)
                $("#image_captcha").attr('src', 'data:image/jpeg;base64,' + result.valicode);
        }
    });
});