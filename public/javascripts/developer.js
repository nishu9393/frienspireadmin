jQuery(document).ready(function () {
    // set local timezone offset in cookies
    var time_zone_offset = -(new Date().getTimezoneOffset());
    $.cookie("time_zone_offset", time_zone_offset);
    // add methods for validation
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, "Invalid file Size");
    $.validator.addMethod('min_filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size >= param)
    }, "Invalid file Size");
    $.validator.addMethod("match", function (value, element, param) {
        return this.optional(element) || param.test(value);
    }, "This field must contain only letters, numbers, space or dashes");
    $.validator.addMethod("regex", function (value, element, regexpr) {
        return regexpr.test(value);
    }, "Enter a valid");
    $("#email").focus();
    $("#password").focus();

    $(".alert").fadeOut(10000);
    $('#getEmail-form').validate({
        focusInvalid: false,
        onkeyup: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 50,
                match: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
                //                match: /^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|biz|com|edu|gov|info|net|org)$/
            }
        },
        messages: {
            email: {
                required: "Enter e-mail.",
                maxlength: "E-mail maximum length is 50.",
                match: "Enter valid e-mail.",
            }
        }
    });
    $('.login-form').validate({
        focusInvalid: false,
        onkeyup: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 50,
                match: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
                //                match: /^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|biz|com|edu|gov|info|net|org)$/
            },
            password: {
                required: true
            },
            captcha: {
                required: true
            }
        },
        messages: {
            email: {
                required: "Enter e-mail.",
                maxlength: "E-mail maximum length is 50.",
                match: "Enter valid e-mail.",
            },
            password: {
                required: "Enter password."
            },
            captcha: {
                required: "Enter captcha code."
            }
        }
    });
    $(".forgot-form").validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                maxlength: 50,
                match: /^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|biz|com|edu|gov|info|net|org)$/
            }
        },
        messages: {
            email: {
                required: "Enter e-mail.",
                maxlength: "E-mail maximum length is 50.",
                match: "Enter valid e-mail."
            }
        }
    });
    $('.changePassword-form').validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            currentPassword: {
                required: true
            },
            newPassword: {
                required: true,
                rangelength: [8, 16],
                match: /^(?=.*\d)(?=[\w!@#$%^&*()+]{8,})(?:.*[!@#$%^&*()+]+.*).*$/
            },
            confirmPassword: {
                required: true,
                equalTo: "#newPassword"
            }
        },
        messages: {
            currentPassword: {
                required: "Enter current password."
            },
            newPassword: {
                required: "Enter new password.",
                rangelength: "Enter a password between 8 to 16 characters. Your password should include letters, numbers and special characters.",
                match: "Enter a password between 8 to 16 characters. Your password should include letters, numbers and special characters."
            },
            confirmPassword: {
                required: "Enter confirm password.",
                equalTo: "The passwords you have entered do not match."
            }
        }
    });

    $('.resetPwd-form').validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            password: {
                required: true,
                rangelength: [8, 16],
                match: /^(?=.*\d)(?=[\w!@#$%^&*()+]{8,})(?:.*[!@#$%^&*()+]+.*).*$/
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Enter new password.",
                rangelength: "Enter a password between 8 to 16 characters. Your password should include letters, numbers and special characters.",
                match: "Enter a password between 8 to 16 characters. Your password should include letters, numbers and special characters."
            },
            password_confirmation: {
                required: "Confirm new password.",
                equalTo: "The passwords you have entered do not match."
            }
        }
    });

    //form validation for category-add-Edit
    $(".category-form").validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            name: {
                required: true,
                rangelength: [3, 100],
                match: /^[a-zA-Z ]*$/,
            }
        },
        messages: {
            name: {
                required: "Enter category name.",
                rangelength: "Enter category name between 3 to 100 characters.",
                match: "Enter only letters."
            }

        }
    });
    $(".profile-form").validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            firstName: {
                match: /^[a-z,0-9,'-]+$/i,
                rangelength: [3, 20],
                required: true
            },
            lastName: {
                match: /^[a-z,0-9,'-]+$/i,
                rangelength: [3, 20],
                required: true
            },
            photo: {
                extension: "jpg|jpeg|png",
                filesize: 10000000
            }
        },
        messages: {
            firstName: {
                required: "Enter first name.",
                rangelength: "Enter first name between 3 to 20 characters.",
                match: "Enter only numbers and letters."
            },
            lastName: {
                required: "Enter last name.",
                rangelength: "Enter last name between 3 to 20 characters.",
                match: "Enter only numbers and letters."
            },
            photo: {
                extension: "Supported Image format jpeg, png.",
                filesize: "Maximum file size 10 MB."
            }
        }
    });
    //form validation for disable user
    $("#disableUser-form").validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            reason: {
                required: true,
                maxlength: 300
            }
        },
        messages: {
            reason: {
                required: "Enter reason.",
                maxlength: "Enter reason max 300 characters only."
            }
        }
    });
    //form validation for settings
    $("#settings-form").validate({
        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                validator.errorList[0].element.focus();
            }
        },
        rules: {
            android_version: {
                required: true,
                number: true,
                max:5
            },
            ios_version: {
                required: true,
                number: true,
                max:5
            }
            
        },
        messages: {
            android_version: {
                required: "Android version is required.",
                number: "Please enter number.",
                max: "You can enter max 5 numbers."
            },
            ios_version: {
                required: "IOS version is required.",
                number: "Please enter number.",
                max: "You can enter max 5 numbers."
            }
           
        }
    });
    // $("#settings-form").validate({
    //     focusInvalid: false,
    //     invalidHandler: function (form, validator) {
    //         var errors = validator.numberOfInvalids();
    //         if (errors) {
    //             validator.errorList[0].element.focus();
    //         }
    //     },
    //     rules: {
    //         near_by: {
    //             required: true,
    //             number: true,
    //             max: 1000000
    //         },
    //         algorithm_distance: {
    //             required: true,
    //             number: true,
    //             max: 1000000
    //         },
    //         version: {
    //             required: true,
    //             number: true,
    //             max: 100
    //         }
    //     },
    //     messages: {
    //         near_by: {
    //             required: "Enter search distance in meters.",
    //             number: "Enter the valid distance.",
    //             max: "You can enter max distance 1000 km."
    //         },
    //         algorithm_distance: {
    //             required: "Enter algorithm distance in meters.",
    //             number: "Enter the valid distance.",
    //             max: "You can enter max distance 1000 km."
    //         },
    //         version: {
    //             required: "Enter app version.",
    //             number: "Enter the valid app version.",
    //             max: "You can enter max app version value 100."
    //         }
    //     }
    // });
    $(document).on("click", "#checkAll", function () {
        $(".check").prop('checked', $(this).prop('checked'));
        if ($(this).prop('checked'))
            $(".check").parent().prop('class', 'checked');
        else
            $(".check").parent().prop('class', '');
    });
    $(document).on("click", ".delete", function () {
        var message = "Are you sure you want to delete?";
        if ($("#url").val() === "/faqs/") {
            message = "Are you sure you want to delete the faq?";
        }
        if ($("#url").val() === "/contact_us/") {
            message = "Are you sure you want to delete the query?";
        }
        if ($("#url").val() === "/category/") {
            message = "Are you sure you want to delete the queries category?";
        }
        if ($("#url").val() === "/drive_image/") {
            message = "Are you sure you want to delete the image?";
        }
        if ($("#url").val() === "/pricing_zone/") {
            message = "Are you sure you want to delete the price of zone?";
        }
        if ($("#url").val() === "/states") {
            message = "Are you sure you want to delete the province?";
        }
        if ($("#url").val() === "/cities/") {
            message = "Are you sure you want to delete the zone?";
        }
        if ($("#url").val() === "/pages") {
            message = "Are you sure you want to delete the cms page?";
        }
        if ($("#url").val() === "/reviews") {
            message = "Are you sure you want to delete?";
        }

        var deleteLink = $(this).attr('deleteLink');
        bootbox.confirm(message, function (result) {
            if (result) {
                window.location = deleteLink;
            }
        });
    });
    $('#deleteSelected').click(function () {
        if ($('.check:checked').length == 0)
            bootbox.alert("Select Atleast One!");
        else {
            bootbox.confirm("Are you sure you want to delete?", function (result) {
                if (result) {
                    $("#action").val("delete");
                    $("#frm").submit();
                }
            });
        }
    });
    $(document).on("click", ".change_status", function () {
        var id = $(this).attr("val");
        var status = $(this).attr("value");
        var obj = $(this);
        var user_type = "";
        var url = "";
        if ($("#url").val() == "/users/") {
            user_type = "user";
            url = "/users/changeStatus";
        }
        if ($("#url").val() == "/faqs/") {
            user_type = "FAQ";
            url = "/faqs/changeStatus";
        }
        if ($("#url").val() == "/category/") {
            user_type = "Category";
            url = "/category/changeStatus";
        }
        if ($("#url").val() == "/drive_image/") {
            user_type = "Image";
            url = "/drive_image/changeStatus";
        }

        var msg = "";
        if (status == 0)
            msg = "Are you sure you want to inactivate this " + user_type + "?";
        else
            msg = "Are you sure you want to activate this " + user_type + "?";
        bootbox.confirm(msg, function (result) {
            if (result) {

                $.ajax({
                    url: url,
                    type: "POST",
                    data: { id: id, status: status },
                    dataType: 'JSON',
                    success: function (result) {

                        if (result == 'unauthorised') {
                            window.location = "/login";
                        } else {
                            if ($("#url").val() == "/users/") {
                                window.location = "/users";
                            }
                            if ($("#url").val() == "/faqs/") {
                                window.location = "/faqs";
                            }
                            if ($("#url").val() == "/category/") {
                                window.location = "/category";
                            }
                            if ($("#url").val() == "/drive_image/") {
                                window.location = "/drive_image";
                            }
                        }
                    }
                });
            }
        });
    });

    $('#update').click(function () {
        if ($('.check:checked').length == 0)
            bootbox.alert("Select Atleast One!");
        else {
            bootbox.dialog({
                message: "Click on Active or Inactive button!",
                title: "Update Status",
                buttons: {
                    success: {
                        label: "Active",
                        className: "green",
                        callback: function () {
                            $("#action").val(1);
                            $("#frm").submit();
                        }
                    },
                    main: {
                        label: "Inactive",
                        className: "red",
                        callback: function () {
                            $("#action").val(0);
                            $("#frm").submit();
                        }
                    },
                    buttons: {
                        label: "CANCEL",
                        className: "blue",
                    }
                }
            });
        }
    });
    $(document).on("click", "#filter", function () {
        var flag = 1;

        console.log("insidefilter");
        if ($("#filterUrl").val() == "/users/") {
            if (!$("#search_by").val() && !$("#filter_gender").val() && !$("#filter_status").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        console.log(flag)
        if ($("#filterUrl").val() == "/reviews/") {
            if (!$("#search_by").val() && !$("#sort_type").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        if ($("#filterUrl").val() == "/contact_us/") {
            if (!$("#search_by").val() && !$("#selected_date").val() && !$("#filter_status").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        if ($("#filterUrl").val() == "/faqs/") {
            if (!$("#search_by").val() && !$("#filter_status").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        if ($("#filterUrl").val() == "/faqs/") {
            if (!$("#search_by").val() && !$("#filter_status").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        if ($("#filterUrl").val() == "/pages/") {
            if (!$("#search_by").val() && !$("#sort_type").val() && !$("#sort_field").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }
        if ($("#filterUrl").val() == "/emailTemplate/") {
            if (!$("#search_by").val() && !$("#sort_type").val() && !$("#sort_field").val()) {
                flag = 0;
                bootbox.alert('Select any filter.');
            }
        }


        console.log(flag)
        var check_regx = /[`^~<>"')(?*%$]/;
        if ($("#search_by").val() && check_regx.test($("#search_by").val())) {
            flag = 0;
            //            bootbox.alert('Enter only letters or numbers.');
            $("#search_by_error").text("Enter only letters or numbers.").show();
        }
        if ($("#sort_field").val() != "") {
            if ($("#sort_field").val() && $("#sort_type").val() == '') {
                flag = 0;
                bootbox.alert('Select Sort Type.');
            }
        }

        console.log(flag)
        if (flag === 1) {
            $("#search_by_error").hide();
            var str = $("form").serialize();
            var search;
            var url = $("#filterUrl").val();
            console.log(str);
            console.log(url);
            console.log(search);
            $.ajax({
                url: url + '?' + str,
                type: "GET",
                data: { search: 1 },
                success: function (result) {

                    if (result == 'unauthorised')
                        window.location = "/login";
                    else
                        $("#ajaxResponce").html(result);
                    Metronic.init(); // init metronic core components
                }
            });
        }
    });

    //on change of sort field enable sort type
    // $(document).on('change', "#sort_field", function () {
    //     let sort_field_value = $(this).val();
    //     if (sort_field_value != "") {
    //         $("#sort_type").removeAttr('disabled');
    //     }

    // })

    // $(document).on("click", "#filter", function () {
    //     var flag = 1;
    //     if ($("#filterUrl").val() == "/users/") {
    //         if (!$("#search_by").val() && !$("#sort_type").val() && !$("#sort_field").val() && !$("#created_at").val()) {
    //             flag = 0;
    //             bootbox.alert('Select any filter.');
    //         }
    //     } else if ($("#filterUrl").val() == "/contact_us/") {
    //         if (!$("#search_by").val() && !$("#sort_type").val() && !$("#sort_field").val() && !$("#created_at").val()) {
    //             flag = 0;
    //             bootbox.alert('Select any filter.');
    //         }
    //     } else if ($("#filterUrl").val() == "/cities/") {
    //         if (!$("#search_by").val() && !$("#sort_type").val() && !$("#state_id").val() && !$("#sort_field").val()) {
    //             flag = 0;
    //             bootbox.alert('Select any filter.');
    //         }

    //     } else if ($("#filterUrl").val() == "/pricing_zone/") {
    //         if (!$("#state").val() && !$("#city").val()) {
    //             flag = 0;
    //             bootbox.alert('Select any filter.');
    //         }
    //     } else {
    //         if (!$("#search_by").val() && !$("#sort_type").val() && !$("#sort_field").val()) {
    //             flag = 0;
    //             bootbox.alert('Select any filter.');
    //         }
    //     }

    //     var check_regx = /[`^~<>"')(?*%$]/;
    //     if ($("#search_by").val() && check_regx.test($("#search_by").val())) {
    //         flag = 0;
    //         //            bootbox.alert('Enter only letters or numbers.');
    //         $("#search_by_error").text("Enter only letters or numbers.").show();
    //     } else if ($("#sort_field").val() && $("#sort_type").val() == '') {
    //         flag = 0;
    //         bootbox.alert('Select Sort Type.');
    //     } else if ($("#sort_type").val() && $("#sort_field").val() == '') {
    //         flag = 0;
    //         bootbox.alert('Select Sort By.');
    //     } else if ($(".search_by").val() == '' && $("#sort_field").val() == '') {
    //         flag = 0;
    //         bootbox.alert('Select Sort By or Search to filter.');
    //     }

    //     if (flag === 1) {
    //         $("#search_by_error").hide();
    //         var str = $("form").serialize();
    //         var search;
    //         var url = $("#filterUrl").val();

    //         $.ajax({
    //             url: url + '?' + str,
    //             type: "GET",
    //             data: { search: 1 },
    //             success: function (result) {

    //                 if (result == 'unauthorised')
    //                     window.location = "/login";
    //                 else
    //                     $("#ajaxResponce").html(result);
    //                 Metronic.init(); // init metronic core components
    //             }
    //         });
    //     }
    // });
    $("#clear").click(function () {
        $("#filter_form")[0].reset();
        $('#sort_type').val('');
        $("#search_by_error").hide();
        var url = $("#filterUrl").val();
        $.ajax({
            url: url,
            type: "GET",
            data: { search: 1 },
            success: function (result) {

                if (result == 'unauthorised')
                    window.location = "/login";
                else {
                    $("#ajaxResponce").html(result);
                    Metronic.init(); // init metronic core components
                }
            }
        });
    });
    function isFloat(n) {
        return Number(n) === n && n % 1 !== 0;
    }

    // $(document).on("click", "#export_button_csv", function () {
    //     if ($("#sort_field").val() && $("#sort_type").val() == '') {
    //         bootbox.alert('Select Sort Type.');
    //     } else if ($("#sort_type").val() && $("#sort_field").val() == '') {
    //         bootbox.alert('Select Sort By.');
    //     } else {
    //         var str = $("form").serialize();
    //         var url = $("#export_csv_url").val();

    //         $.ajax({
    //             url: url + '?' + str,
    //             type: "GET",
    //             data: { search: 1 },
    //             success: function (result) {

    //                 if (result == 'unauthorised')
    //                     window.location = "/login";
    //                 else {
    //                     //front end code for export csv
    //                     download(result);
    //                 }
    //             }
    //         });
    //     }
    // });
    // Final Code for Download CSV Function
    function download(json_data1) {
        var _log = json_data1;
        var csvData = ConvertToCSV(_log);
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        var timestamp = Math.floor(Date.now() / 1000);
        a.download = timestamp + '_data.csv';
        a.click();
    }

    // convert Json to CSV data
    function ConvertToCSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = "";
        for (var index in objArray[0]) {
            //Now convert each value to string and comma-separated
            row += index + ',';
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';
        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '')
                    line += ','
                line += '"' + array[i][index] + '"';
            }
            str += line + '\r\n';
        }
        return str;
    }

    $(document).on("click", ".view", function () {
        /***********user ajax view *******/
        var url_for_user_view = '';
        if ($("#url").val() == "/users/") {
            url_for_user_view = "/users/view";
        }

        if ($("#url").val() == "/faqs/") {
            url_for_user_view = "/faqs/view";
        }
        if ($("#url").val() == "/category/") {
            url_for_user_view = "/category/view";
        }
        if ($("#url").val() == "/reviews/") {
            url_for_user_view = "/reviews/view";
        }

        var user_id = $(this).attr("user_id");

        $.ajax({
            url: url_for_user_view,
            type: "POST",
            data: { id: user_id },
            dataType: "JSON",
            success: function (result) {

                if (result == 'unauthorised')
                    window.location = "/login";
                else if (result.status == 'error') {
                    if ($("#url").val() == "/users/") {
                        window.location = "/users";
                    }
                    if ($("#url").val() == "/faqs/") {
                        window.location = "/faqs";
                    }
                    if ($("#url").val() == "/category/") {
                        window.location = "/category";
                    }
                } else {
                    if ($("#url").val() == "/users/") {
                        var status = "Active";
                        $('#name').text(result.data.firstName + " " + result.data.lastName);
                        $('#email').text(result.data.email || "--");
                        $('#phone').text(result.data.phone || "--");
                        if (result.data.status == 3)
                            status = "Blocked";
                        $('#status').text(status);
                        $('#unique_id').text(result.data.unique_id || "--");
                        var addr = '';
                        for (var i = 0; i < result.data.customer_address.length; i++) {
                            addr += '<b>' + result.data.customer_address[i].address_name + '</b>, ' +
                                result.data.customer_address[i].unit_number + ', ' +
                                result.data.customer_address[i].street_name + ', ' +
                                result.data.customer_address[i].sublocation_id.area_name + ', ' +
                                result.data.customer_address[i].location_id.name + '<br>'
                        }
                        $('#address1').html(addr || "Not Available");
                    }

                    if ($("#url").val() == "/faqs/") {
                        $('#question').text(result.data.heading);
                        $('#answer').text(result.data.description);
                    }
                    $('#myModal').modal('show');
                }
            }
        });
        /***********user ajax view ends here*******/
    });
    $(document).on("click", ".disable_user", function () {
        $('#sub_error_getEmail').text("");
        $('.error').text("");
        $('#disableUser-form').trigger("reset");
        var userId = $(this).attr('userid');
        var status = $(this).attr('status');
        $('#disable_user_btn').attr('userid', userId);
        $('#disable_user_btn').attr('status', status);

        if (status == 1)
            $('.disable_reason_popup').html('Add your comment "why you want to Unblock this user?"');
        else
            $('.disable_reason_popup').html('Add your comment "why you want to Block this user?"');
    });
    //disable user or block bottle drive
    $(document).on("click", "#disable_user_btn", function () {
        var url_for_user_disable = "";
        var disable_reason = $('#disableUserID').val();
        var userId = $(this).attr('userid');
        var status = $(this).attr("status");
        if ($('#disableUser-form').valid()) {
            if ($("#url").val() == "/users/") {
                url_for_user_disable = "/users/disable_user";
            }
            console.log(disable_reason);
            $.ajax({
                url: url_for_user_disable,
                type: "POST",
                data: { userId: userId, disable_reason: disable_reason, status: status },
                dataType: "JSON",
                success: function (result) {

                    if (result == 'unauthorised')
                        window.location = "/login";
                    else if (result.status == 1) {
                        $("#sub_error_getEmail").text("");
                        $('#disableUserModal').modal('hide');
                        if ($("#url").val() == "/users/") {
                            if (status == 1) {
                                $('#disable_user_btn').attr("status", 1);
                            } else {
                                $('#disable_user_btn').attr("status", 0);
                            }
                            window.location = "/users";
                        } else if ($("#url").val() == "/bottle_drives/") {
                            window.location = "/bottle_drives";
                        } else if ($("#url").val() == "/reported_bottle_drives/") {
                            window.location = "/reported_bottle_drives";
                        } else if ($("#url").val() == "/bottle_drive_details/") {
                            window.location = $("#back_url").val();
                        }
                    } else {
                        $("#sub_error_getEmail").text(result.message);
                    }
                }
            });
        }
    });
    $(document).on("change", "#sort_field", function () {
        if ($('#sort_field').val()) {
            $('#sort_type').prop('disabled', false);
        } else {
            $('#sort_type').prop('disabled', true);
        }

        if ($('#sort_field').val() === "status") {
            var values = [{ status: -1, name: "Active" }, { status: 1, name: "Inactive" }];
        } else {
            var values = [{ status: 1, name: "Ascending" }, { status: -1, name: "Descending" }];
        }
        var options = '<option value="">--Select--</option>';
        for (var i = 0; i < values.length; i++) {
            options += '<option value="' + values[i].status + '">' + values[i].name + '</option>';
        }
        $('#sort_type').html(options);
    });
    $(document).on("change", "#state", function () {
        if ($('#state').val()) {

            $.ajax({
                url: "/pricing_zone/cities/" + $(this).val(),
                type: "GET",
                dataType: "JSON",
                success: function (result) {

                    if (result == 'unauthorised')
                        window.location = "/login";
                    else if (result.status == 1) {
                        var cities = '<option value="">--Select--</option>';
                        for (var i = 0; i < result.cities.length; i++) {
                            cities += '<option value="' + result.cities[i]._id + '">' + result.cities[i].name + '</option>'
                        }
                        $('#city').html(cities);
                    }
                }
            });
            $('#city').prop('disabled', false);
        } else {
            $('#city').val("");
            $('#city').prop('disabled', true);
        }
    });
    $(document).on("click", "#start_date, input[name='start_date']", function (e) {
        $("#start_date_picker").datepicker('show').on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#end_date_picker').datepicker('setDate', "");
            $('#end_date_picker').datepicker('setStartDate', minDate);
        });
    });

    $(document).on("click", "#end_date, input[name='end_date']", function (e) {
        $("#end_date_picker").datepicker('show');
    });

    $(document).keydown(function (e) {
        // ESCAPE key pressed
        if (e.keyCode === 27) {
            $('.modal').modal('hide');
        }
    });
    //    $('.password_field').bind("cut copy paste", function (e) {
    //        e.preventDefault();
    //        $('#password').bind("contextmenu", function (e) {
    //            e.preventDefault();
    //        });
    //    });


    //view user details
    $(document).on("click", ".view-user", function (e) {
        var href = $(this).attr("link");
        console.log(href);
        $.ajax({
            url: href,
            type: "GET",
            dataType: "JSON",
            success: function (result) {

                if (result == 'unauthorised')
                    window.location = "/login";
                else if (result.status == 1) {
                    console.log("result");
                    console.log(result);
                    let user = $("#user_details_table");
                    if (result.userDetailData.profile_pic == "no-image" || result.userDetailData.profile_pic == null || result.userDetailData.profile_pic == '') {
                        user.find(".profile_pic .single_image").attr("href", "/images/default.png");
                        user.find(".profile_pic .img-circle").attr("src", "/images/default.png");
                    }
                    else {
                        user.find(".profile_pic .single_image").attr("href", result.userDetailData.profile_pic);
                        user.find(".profile_pic .img-circle").attr("src", result.userDetailData.profile_pic);
                    }
                    if (!result.userDetailData.firstName) {
                        result.userDetailData.firstName = "--"
                    }
                    console.log(user.find(".first_name").text(result.userDetailData.firstName));
                    if (!result.userDetailData.lastName) {
                        result.userDetailData.lastName = "--"
                    }
                    user.find(".last_name").text(result.userDetailData.lastName);
                    if (result.userDetailData.gender == "") {
                        result.userDetailData.gender = "--"
                    }

                    let gender;
                    if (result.userDetailData.gender == 1) {
                        gender = "Male";
                    }
                    else if (result.userDetailData.gender == 2) {
                        gender = "Female";
                    }
                    else {
                        gender = "Others";
                    }
                    user.find(".gender").text(gender);
                    if (!result.userDetailData.email) {
                        result.userDetailData.email = "--"
                    }
                    user.find(".email").text(result.userDetailData.email);
                    /*     if((!result.userDetailData.dob)  || (result.userDetailData.dob == ""){ */
                    if (!result.userDetailData.dob) {
                        user.find(".dob").text("--");
                    } else {
                        let dob = new Date(result.userDetailData.dob);
                        user.find(".dob").text(dob.getDate() + "/" + (dob.getMonth() + 1) + "/" + dob.getFullYear());
                    }
                    if (!result.userDetailData.location) {
                        result.userDetailData.location = "--"
                    }
                    user.find(".location").text(result.userDetailData.location);
                    $("#user-details").modal('show');

                }
            }
        });
    });


    //view reported reviews details
    $(document).on("click", ".view-contact-us", function (e) {
        var href = $(this).attr("link");
        console.log(href);
        $.ajax({
            url: href,
            type: "GET",
            dataType: "JSON",
            success: function (result) {
                console.log("success");
                console.log(result);
                console.log(result.status);
                if (result == 'unauthorised')
                    window.location = "/login";
                else if (result.status == 1) {
                    console.log("result...........");
                    console.log(result);
                    let query = $("#query_details_table");
                    query.find(".username").text(result.query_data.data[0].user.firstName + " " + result.query_data.data[0].user.lastName);
                    query.find(".date_time").text(result.query_data.data[0].created_at);
                    query.find(".subject").text(result.query_data.data[0].contact.name);
                    query.find(".description").text(result.query_data.data[0].message);
                    let id = result.query_data.data[0]._id;
                    console.log(id);
                    if (result.query_data.data[0].status == 0) {
                        let create_resolve_link = ` <div class="resolved"> <a id='my_anchor' linkdata='/contact_us/update/` + id + `'><b>Mark as resolved</b></a>
                        <input type='checkbox' id='checkbox_switch'> </div>` ;
                        $("#resolve_container").html(create_resolve_link);
                    }

                    $("#query-details").modal('show');


                }
            }
        });
    })


    //onclick checkbox of contact_us details  href of deleted routed get triggered

    $(document).on("click", "#checkbox_switch", function (e) {
        console.log("inside update");
        let href = $("#my_anchor").attr("linkdata");
        $(this).parents('#resolve_container').empty();

        window.location = href;
    })

});




