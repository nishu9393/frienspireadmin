var express = require('express');
var router = express.Router();
const controller = require("../controllers/userController");



/* GET method for list and search Users  */
router.get('/', controller.get_users);

// get user details
router.get('/user_detail/:id', controller.get_user_detail);

// disable user
router.post('/disable_user', controller.post_disable_user);


module.exports = router;

