var express = require('express');
var router = express.Router();
const controller = require("../controllers/reviewController");

// get all reported reviews
router.get("/",controller.reported_reviews_get);

// get review details details
router.get('/review_detail/:id',controller.get_review_detail);
//detele the review
router.get('/delete/:id',controller.delete_review);
//make review appropriate 
router.get("/appropriate/:id",controller.mark_appropriate)


module.exports = router;